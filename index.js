let sum = 0;
let n;

for (n = 1; sum <= 10000; n++) {
  sum += n;
}

document.getElementById('soNguyenDuong').innerHTML ='Số nguyên dương nhỏ nhất: ' + n;

function tinhTong(){
    x = document.getElementById('soX').value;
    n = document.getElementById('soN').value;
    let sum = 0;
    for (let i = 1; i <= n; i++) {
        sum += Math.pow(x, i);
    }
    document.getElementById('tong').innerHTML = 'Tổng: ' + sum;
}

function giaiThua(){
    n = document.getElementById('soNGiaiThua').value;
    for (let i = n - 1; i > 0; i--) {
        n *= i;
    }
    document.getElementById('giaiThua').innerHTML = 'Giai thừa: ' + n;
}

function taoDiv(){
    for (let i = 1; i <= 10; i++) {
        if(i % 2 == 0){
            document.getElementById('div-container').innerHTML += '<div class="bg-danger text-white p-2">Div chẳn ' + i + '</div>';
        }else{
            document.getElementById('div-container').innerHTML += '<div class="bg-primary text-white p-2">Div lẻ ' + i + '</div>';
        }

    }
}